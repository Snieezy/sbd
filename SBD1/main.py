# sortowanie niemalejaco
import tape
import Record


def get_smaller_least_common_value(record1, record2):
    if record1.least_common_value() < record2.least_common_value():
        return record1
    else:
        return record2


def is_end_of_run(previous_record, current_record):
    prev = previous_record.least_common_value()
    cur = current_record.least_common_value()
    if prev <= cur:
        return False
    return True


def get_next_record(number_of_tape, line_number):
    i = 1
    tape_file = open("tape"+str(number_of_tape), 'r')
    for line in tape_file:
        if i == line_number:
            tape_file.close()
            return line
        i += 1


def switch_number_of_tape(number_of_tape):
    if number_of_tape == 2:
        return 3
    else:
        return 2


def merge_tapes(number_of_tape1, number_of_tape2):
    # TODO

    return


def tape_redistribution():
    number_of_tape = 2
    tape_file = open("tape1", 'r')
    previous_line = Record.Record()
    previous_line.read_line(tape_file)
    previous_line.write_to_file(number_of_tape)
    current_line = Record.Record()
    while current_line.read_line(tape_file):

        print previous_line.get_values()
        print current_line.get_values()

        if is_end_of_run(previous_line, current_line):
            number_of_tape = switch_number_of_tape(number_of_tape)
            if not current_line.is_record_used():
                current_line.write_to_file(number_of_tape)
            previous_line.set_values(current_line.get_values())
        else:
            if not current_line.is_record_used():
                current_line.write_to_file(number_of_tape)
            previous_line.set_values(current_line.get_values())
    tape_file.close()

tape_redistribution()
