import re


class Record:
    _values = []
    _is_used = False

    def __init__(self):
        self._values = [0, 0, 0]

    def set_values(self, values):
        self._values = values

    def get_values(self):
        return self._values

    def set_record_used(self):
        self._is_used = True

    def is_record_used(self):
        return self._is_used

    def clean_use_stat(self):
        self._is_used = False

    def write_to_file(self, number_of_tape):
        tape_file = open("tape"+str(number_of_tape), "a")
        for item in self._values:
            tape_file.write("%s " % item)
        tape_file.write("\n")
        tape_file.close()
        self._is_used = True

    def least_common_value(self):
        values = set([abs(int(v)) for v in self._values])
        if values and 0 not in values:
            n = n0 = max(values)
            values.remove(n)
            while any(n % m for m in values):
                n += n0
            return n
        return 0

    def read_line(self, tape_file):
        self._values = tape_file.readline()
        self.fix_line()
        self._is_used = False

        if self._values != ['']:
            return True
        else:
            return False

    def fix_line(self):
        self._values = re.sub('\n', '', self._values)
        self._values = self._values.split(" ")

