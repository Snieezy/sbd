class Tape:
    name = ""
    mode = ""

    def __init__(self, tape_name, tape_mode):
        self.name = tape_name
        self.mode = tape_mode

    def clear_tape(self):
        open(self.name, 'w').close()

